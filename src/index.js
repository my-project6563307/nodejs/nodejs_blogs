const express = require("express");
const path = require("path");
const morgan = require("morgan");
const handlebars = require("express-handlebars");
const app = express();
const port = 3000;
// config static path
app.use(express.static(path.join(__dirname, "public")));
// http logger
app.use(morgan("combined"));

// Template engines
app.engine(
    "hbs",
    handlebars.engine({
        extname: ".hbs",
    })
);
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "resources/views"));

app.get("/", (req, res) => {
    res.render("home");
    res.render("news");
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
